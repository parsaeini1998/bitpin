from django.urls import path

from . import views

app_name = "contents"

urlpatterns = [
    path("", views.ContentListAPIView.as_view(), name="content-list"),
    path("rate/", views.ContentRateAPIView.as_view(), name="content-rate"),
]
