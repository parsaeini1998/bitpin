from django.contrib import admin

from contents.models import Content, ContentRateLog


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ("title", "uid")
    readonly_fields = ("uid",)


@admin.register(ContentRateLog)
class ContentRateLogAdmin(admin.ModelAdmin):

    list_display = ("user", "rate")
