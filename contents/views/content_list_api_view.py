from django.db.models import Count, Avg
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from contents.models import Content
from contents.serializers import ContentSerializer


class ContentListAPIView(ListAPIView):
    serializer_class = ContentSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return Content.objects.all().annotate(
            rates_count=Count("content_rate_logs"),
            rates_avg=Avg("content_rate_logs__rate"),
        )
