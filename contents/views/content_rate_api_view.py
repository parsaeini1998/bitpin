from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

from contents.serializers import ContentRateSerializer


class ContentRateAPIView(CreateAPIView):
    serializer_class = ContentRateSerializer
    permission_classes = (IsAuthenticated,)
