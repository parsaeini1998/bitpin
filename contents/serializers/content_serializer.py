from rest_framework import serializers

from contents.models import Content


class ContentSerializer(serializers.ModelSerializer):
    rates_count = serializers.IntegerField()
    rates_avg = serializers.IntegerField()

    class Meta:
        model = Content
        fields = ("uid", "title", "text", "rates_count", "rates_avg")
