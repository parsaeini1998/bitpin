from django.core.validators import MaxValueValidator, MinValueValidator
from django.shortcuts import get_object_or_404
from rest_framework import serializers

from contents.models import ContentRateLog, Content


class ContentRateSerializer(serializers.Serializer):
    rate = serializers.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    content_uid = serializers.UUIDField(write_only=True)

    def create(self, validated_data):
        user = self.context.get("request").user
        content = get_object_or_404(Content, uid=validated_data.pop("content_uid"))
        content_rate_log, _ = ContentRateLog.objects.update_or_create(
            user=user,
            content=content,
            defaults={"rate": validated_data.get("rate")},
        )

        return content_rate_log

    def update(self, instance, validated_data):
        return super(ContentRateSerializer, self).update(instance, validated_data)
