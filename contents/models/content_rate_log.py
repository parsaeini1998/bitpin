from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


class ContentRateLog(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name=_("user"),
        on_delete=models.PROTECT,
    )
    rate = models.PositiveIntegerField(verbose_name=_("rate"))
    content = models.ForeignKey(
        "contents.Content",
        on_delete=models.CASCADE,
        related_name="content_rate_logs",
    )

    def __str__(self):
        return f"{self.user}, {self.rate}, {self.content.title}"

    class Meta:
        verbose_name = _("content rate log")
        verbose_name_plural = _("content rate logs")
