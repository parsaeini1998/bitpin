from django.db import models
from django.utils.translation import gettext_lazy as _
from uuid import uuid4


class Content(models.Model):
    uid = models.CharField(
        max_length=36, default=uuid4, db_index=True, verbose_name=_("content uid")
    )
    title = models.CharField(max_length=256, verbose_name=_("title"))
    text = models.TextField(max_length=2056, verbose_name=_("text"))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("content")
        verbose_name_plural = _("contents")
