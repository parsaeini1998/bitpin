## setup


- Create a postgres database with related configurations in settings.py file
- Run python manage.py migrate
- Run python manage.py loaddata fixtures/users.json
- Run python manage.py loaddata fixtures/contents.json
- ready to go!