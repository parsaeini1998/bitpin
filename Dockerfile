FROM python:3.8-slim
WORKDIR /srv
COPY . .
RUN pip install --upgrade pip
ADD requirements.txt /srv/
RUN pip install -r ./requirements.txt
ADD . /srv/
ADD ./fixtures /srv/fixtures
CMD python manage.py makemigrations
CMD python manage.py migrate
CMD python manage.py loaddata fixtures/users.json
CMD python manage.py loaddata fixtures/contents.json
CMD python manage.py runserver 0.0.0.0:7070

